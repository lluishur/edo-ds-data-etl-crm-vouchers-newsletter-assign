FROM python:3.5-slim

ADD .config /root/.config

RUN pip install --upgrade pip


RUN apt-get update && apt-get install -y curl lsb-release gnupg dos2unix && \
    echo deb http://packages.cloud.google.com/apt cloud-sdk-$(lsb_release -c -s) main | \
    tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update && apt-get install -y google-cloud-sdk && \
    apt-get autoremove --purge -y && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*


COPY ./requirements.txt ./requirements.txt

RUN python -m pip install -r requirements.txt

ADD processes /app/processes
ADD python /python

RUN chmod u+x /app/processes/run.sh

ENTRYPOINT ["bash","/app/processes/run.sh"]
