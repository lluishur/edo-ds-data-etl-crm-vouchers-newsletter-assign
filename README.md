# edo-ds-data-etl-crm-vouchers-newsletter-assign

conda environment: edo-ds-data-etl-crm-vouchers-newsletter-assign

Buckets en el proyecto  edo-dev-ds-datalake

edo_ds_data_etl_ds_team_crm_vouchers_newsl_main_stage_file_dev - donde van a estar los ficheros csv que creastes tu (los 4 por país)

edo_ds_data_etl_ds_team_crm_vouchers_newsl_main_ftp_to_gcs_dev - donde vas a copiar el fichero del ftp

edo_ds_data_etl_ds_team_crm_vouchers_newsl_main_processed_dev - donde vas a generar el fichero final, y desde donde será enviado al ftp

ya te los he dado acceso para leer y escribir así que puedes enviar los ficheros y copiarlos desde allí con el comando gsutil ...

Introducir en BQ
-Input file from sftp
-Output file sent by this ETL

Generar un query que tome user id's (en los dos archivos anteriores) + utm from GA + variables de GA para calcular el 
P2B y producir el output necesario para analizar el test y guardamos el output en BigQuery. Esta query se ejecutará 
cuando termine el test, y a partir de entonces cada día.

STEPS

0. Create BigQuery table (fullVisitor customer) with fullVisitorId + email + market for all markets up to a certain day.
Then, query the existing table every day.

1. The file of NL customers (NL_df) is provided via ftp with the name

NL_BoxeverID_YYYY_MM_DD.csv

Columns: SubscriberKey, email, market, date

This file is sent around 4 times per week. The ETL must check if the file exists and execute only then.

Write in bucket

2. Move NL file to BigQuery

3. Query the NL_df from BigQuery.
nl_customers.sql

4. Query the fullVisitorId table from step 0. Add new possible users. New users exist from the day of last execution
 till the present. 
 
[NO] Alternative1: the fullVisitor customers table in BigQuery is updated everyday. 

[NO] Alternative2: use table historical_user_ids in ds_prime. Needs to be completed. Is updated automatically.

[YES] Alternative3: create a temporal table with GA users from all markets: fullVisitorId + email + market

5. Segregate the markets.

markets = opodo.es,edreams.es,opodo.pt,edreams.pt,travellink.de,edreams.de,opodo.de,edreams.it,opodo.it,govoyages.com,travellink.com,edreams.com,opodo.com,edreams.fr,govoyages.fr,opodo.fr,edreams.uk,opodo.uk

Assign a voucher according to specified criterium. If so, the ETL needs to query BQ and/or call other packages.

vouchers = [VoucherA, VoucherB, VoucherC]

See processCSV.py

6. Upload results to BigQuery

7. Send resulting data frame via sftp

data frame columns: fullVisitorId, SubscriberKey, email, market, date, Voucher
