from datetime import datetime
from airflow.models import DAG, Variable
from dags.crm_dags.vouchers_newsletter_assign_maker import CrmVouchersNewsletterAssignDag, DAG_ID

bucket_in = Variable.get("BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_INPUT")
bucket_out = Variable.get("BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_OUTPUT")
sftp_path = Variable.get("SFTP_CRM_GENERAL")
sftp_folder = Variable.get("FOLDER_SFTP_CRM_GENERAL")
project = Variable.get("PROJECT")
google_secret = Variable.get("google-secret")
crm_ftp_secret = Variable.get("crm-ftp-secret")

main_dag = DAG(
    dag_id=DAG_ID,
    schedule_interval="30 10 * * *",
    start_date=datetime(2020, 02, 16),
    catchup=False
)

merge_vouchers_optimization_assign = CrmVouchersNewsletterAssignDag(). \
    make_subdag(overdag=main_dag,
                bucket_in=bucket_in,
                bucket_out=bucket_out,
                sftp_path=sftp_path,
                sftp_folder=sftp_folder,
                project=project,
                google_secret=google_secret,
                crm_ftp_secret=crm_ftp_secret
    )