from edo.dagger.dag_maker import DagMaker

DAG_ID = "CRM_VOUCHERS_NEWSLETTER_ASSIGN"

class CrmVouchersNewsletterAssignDag(DagMaker):
    def __init__(self, prefix=DAG_ID):
        super().__init__(prefix=prefix, name_params=[])
