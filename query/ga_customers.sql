SELECT
  market,
  CASE
      WHEN website = 'OPES' THEN 'opodo.es'
      WHEN website = 'OPPT' THEN 'opodo.pt'
      WHEN website = 'TRDE' THEN 'travellink.de'
      WHEN website = 'GOES' THEN 'govoyages.es'
      WHEN website = 'GOIT' THEN 'govoyages.it'
      WHEN website = 'GOPT' THEN 'govoyages.pt'
      WHEN website = 'GODE' THEN 'govoyages.de'
      WHEN website = 'GOGB' THEN 'govoyages.com'
      WHEN website = 'TRGB' THEN 'travellink.com'
      WHEN website = 'DE' THEN 'edreams.de'
      WHEN website = 'ES' THEN 'edreams.es'
      WHEN website = 'FR' THEN 'edreams.fr'
      WHEN website = 'GB' THEN 'edreams.com'
      WHEN website = 'IT' THEN 'edreams.it'
      WHEN website = 'PT' THEN 'edreams.pt'
      WHEN website = 'UK' THEN 'edreams.uk'
      WHEN website = 'GOFR' THEN 'govoyages.fr'
      WHEN website = 'OPDE' THEN 'opodo.de'
      WHEN website = 'OPFR' THEN 'opodo.fr'
      WHEN website = 'OPGB' THEN 'opodo.com'
      WHEN website = 'OPIT' THEN 'opodo.it'
      WHEN website = 'OPUK' THEN 'opodo.uk'
      ELSE website
      END AS website,
  email,
  fullVisitorId,
  date_time
FROM `edo-dev-ds-datalake.etl_general_data_ds.ga_customers_ids`

--SELECT
--  market,
--  website,
--  email,
--  fullVisitorId,
--  date_time
--FROM `edo-dev-ds-datalake.etl_general_data_ds.ga_customers_ids`
-- WHERE DATE(date_time, "Europe/Berlin") < '2018-01-19'
