#!/usr/bin/env bash
# Download data from SFTP + Save in GCS + Process + Upload data to GCS + Delete original file in GCS

DATE=$1
FILENAME=$2
FILENAMEOUT=$3
EXPLORATION_PHASE=$4
MARKET_LIST=$5
VOUCHER_LIST=$6

echo "--------------------------------------"
echo "DATE: $DATE"
echo "FILENAME: $FILENAME"
echo "FILENAMEOUT: $FILENAMEOUT"
echo "EXPLORATION_PHASE: $EXPLORATION_PHASE"
echo "MARKET_LIST: $MARKET_LIST"
echo "VOUCHER_LIST: $VOUCHER_LIST"

DATE_FOLDER=$(date -d "$DATE" +"%Y/%m/%d")

ETL_PROJECT="crm_platform"
ETL_EVENT="vouchers_newsletter_assign"
ETL_TASK="aux_query"

URL="http://$PIPELINER_SERVER/schema-events/event/$ETL_PROJECT/$ETL_EVENT/$ETL_TASK/$DATE"
echo "Querying URL: $URL"
status_code=$(curl --write-out %{http_code} --silent --output /dev/null $URL)
curl -w "%{http_code}\n" $URL ; echo "Exit code: $?"
echo "Http status code: $status_code"
if [[ "$status_code" -ne 200 ]] ; then
    echo "Couldn't get data from pipeliner, aborting process"
    exit 1
fi

curl $URL -o /tmp/sql.txt

dos2unix /tmp/sql.txt || { echo "sql.txt doesn't exist"; exit 1; }

QUERY=$(cat /tmp/sql.txt)

INPUT_PATH=$BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_INPUT/$DATE_FOLDER/$FILENAME
USERS_BUCKET=$BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_INPUT
USERS_FOLDER=$DATE_FOLDER
USERS_PATH=$USERS_BUCKET/$USERS_FOLDER
OUTPUT_PATH=$BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_OUTPUT/$DATE_FOLDER/$FILENAMEOUT

python3 /python/processCSV.py "$INPUT_PATH" "$USERS_BUCKET" "$USERS_FOLDER" "$OUTPUT_PATH" "$QUERY" "$EXPLORATION_PHASE" "$MARKET_LIST" "$VOUCHER_LIST"
#python3 /python/processCSV.py "$GOOGLE_APPLICATION_CREDENTIALS" "$INPUT_BUCKET" "$USERS_BUCKET" "$OUTPUT_BUCKET" "$QUERY"

#ERASE files from bucket
gsutil rm -r $USERS_PATH/*
gsutil rm -r $BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_INPUT/$DATE_FOLDER/*