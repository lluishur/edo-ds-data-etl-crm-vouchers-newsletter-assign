#!/usr/bin/env bash
# Export data from GCS to BQ

DATE=$1
FILENAMEOUT=$2

echo "--------------------------------------"
echo "DATE: $DATE"
echo "FILENAMEOUT: $FILENAMEOUT"

DATE_FOLDER=$(date -d "$DATE" +"%Y/%m/%d")

ETL_PROJECT="crm_platform"
ETL_EVENT="vouchers_newsletter_assign"
ETL_TASK="output_schema"

URL="http://$PIPELINER_SERVER/schema-events/event/$ETL_PROJECT/$ETL_EVENT/$ETL_TASK/$DATE"
echo "Querying URL: $URL"
status_code=$(curl --write-out %{http_code} --silent --output /dev/null $URL)
curl -w "%{http_code}\n" $URL ; echo "Exit code: $?"
echo "Http status code: $status_code"
if [[ "$status_code" -ne 200 ]] ; then
    echo "Couldn't get data from pipeliner, aborting process"
    exit 1
fi

curl $URL -o /tmp/schema.txt

dos2unix /tmp/schema.txt || { echo "schema.txt doesn't exist"; exit 1; }

FULL_TABLE_NAME=$PROJECT:$CRM_VOUCHERS_NEWSLETTER_ASSIGN_DATASET.$CRM_VOUCHERS_NEWSLETTER_ASSIGN_OUTPUT_TABLE
FULL_BUCKET_NAME=$BUCKET_CRM_VOUCHERS_NEWSLETTER_ASSIGN_OUTPUT/$DATE_FOLDER/$FILENAMEOUT

bq load \
--source_format=CSV \
--time_partitioning_field date \
$FULL_TABLE_NAME \
$FULL_BUCKET_NAME \
/tmp/schema.txt
